import createFastify from 'fastify';
import fastifyStatic from '@fastify/static';
import axios from 'axios';
import { JSDOM } from 'jsdom';

import {
    port,
    origin
} from './config.js';

const fastify = createFastify({
    maxParamLength: 2000
});

fastify.register(
    fastifyStatic,
    {
        root: new URL('./public', import.meta.url).pathname,
        prefix: '/public/'
    }
);

fastify.get(
    '/',
    (
        request,
        reply
    ) => {
        reply.sendFile('main.html');
    }
);

for(const method of [
    'DELETE',
    'GET',
    'PATCH',
    'POST',
    'PUT'
]) fastify.route({
    method,
    url: '/:data',
    handler: async (
        {
            params: {
                data: requestData
            },
            headers: requestHeaders,
            body: requestBody
        },
        reply
    ) => {
        const
            {
                url,
                isCss,
                isJs
            } = JSON.parse(Buffer.from(requestData, 'base64').toString('utf8')),
            {
                data: responseBody,
                headers: responseHeaders
            } = await axios({
                method,
                url,
                headers: {
                    ...requestHeaders,
                    'referer': requestHeaders['referer'] && !requestHeaders['referer'].startsWith('http')
                        ? JSON.parse(Buffer.from(new URL(requestHeaders['referer']).pathname.slice(1), 'base64').toString('utf8')).url
                        : undefined,
                    'host': undefined
                },
                data: requestBody,
                responseType: 'arraybuffer'
            });
        reply.headers({
            ...responseHeaders,
            'X-Frame-Options': undefined
        });
        if(responseHeaders['content-type'].startsWith('text/html')){
            const { window } = new JSDOM(new TextDecoder().decode(responseBody));
            for(const attribute of [
                'action',
                'cite',
                'data',
                'formaction',
                'href',
                'longdesc',
                'manifest',
                'poster',
                'src'
            ]){
                for(const element of window.document.querySelectorAll(`[${attribute}]`)){
                    let elementUrl;
                    try {
                        elementUrl = new URL(element.getAttribute(attribute), url);
                    }
                    catch {}
                    if(elementUrl && ['https:', 'http:'].includes(elementUrl.protocol)){
                        const data = Buffer.from(JSON.stringify({
                            url: elementUrl,
                            isCss,
                            isJs
                        }), 'utf8').toString('base64');
                        element.setAttribute(
                            attribute,
                            `${origin}/${data}`
                        );
                        element.removeAttribute('target');
                    }
                }
            }
            if(isCss === false){
                for(const element of window.document.querySelectorAll('style, link[rel="stylesheet"]'))
                    element.remove();
                for(const element of window.document.querySelectorAll('[style]'))
                    element.removeAttribute('style');
            }
            if(isJs === false){
                for(const element of window.document.querySelectorAll('script'))
                    element.remove();
            }
            {
                const scriptElement = window.document.createElement('script');
                scriptElement.setAttribute('src', '/public/iframe.js');
                window.document.head.prepend(scriptElement);
            }
            reply.send(window.document.documentElement.innerHTML);
        }
        else
            reply.send(responseBody);
    }
});

fastify
    .listen({
        port
    })
    .then(() => console.log(`Listening to localhost:${port}`))
    .catch(console.error);