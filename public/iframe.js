const {
    url,
    isEruda
} = JSON.parse(atob(window.location.pathname.slice(1)));

{
    const xhrOpen = window.XMLHttpRequest.prototype.open;
    window.XMLHttpRequest.prototype.open = function(){
        const _url = new URL(arguments[1], url);
        if(['https:', 'http:'].includes(_url.protocol))
            arguments[1] = `/${btoa(JSON.stringify({ url: _url.href }))}`;
        return xhrOpen.apply(this, arguments);
    };
}

{
    const fetch = window.fetch;
    window.fetch = function(){
        const _url = new URL(arguments[0], url);
        if(['https:', 'http:'].includes(_url.protocol))
            arguments[0] = `/${btoa(JSON.stringify({ url: _url.href }))}`;
        return fetch.apply(this, arguments)
    }
}

if(isEruda){
    const erudaScriptElement = document.createElement('script');
    erudaScriptElement.setAttribute('src', 'https://cdn.jsdelivr.net/npm/eruda@2.11.2');
    erudaScriptElement.addEventListener('load', () => window.eruda.init());
    document.head.appendChild(erudaScriptElement);
}